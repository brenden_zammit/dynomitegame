package Player;
import MainInit.*;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.math.Vector3f;

public class Player {
    
    private static BulletAppState bulletAppState;
    private static CharacterControl playerControl;
    
    public static CharacterControl initPlayer(CharacterControl playerControl, BulletAppState bulletAppState){
        Player.bulletAppState = bulletAppState;
        Player.playerControl = playerControl;
        control();
        
        return Player.playerControl;
    }
    
    private static void control() {
        CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(2, 6, 1);
        playerControl = new CharacterControl(capsuleShape, 0.01f);
        playerControl.setJumpSpeed(20);
        playerControl.setFallSpeed(30);
        playerControl.setGravity(30);
        playerControl.setPhysicsLocation(new Vector3f(4, 5, 4));

        bulletAppState.getPhysicsSpace().add(playerControl);
    }
}
