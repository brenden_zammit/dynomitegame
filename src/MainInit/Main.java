package MainInit;
import Level.*;
import Player.Player;
import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;

public class Main extends SimpleApplication implements ActionListener {

    private BulletAppState bulletAppState;
    private CharacterControl playerControl;
    private Vector3f walkDirection = new Vector3f();
    private boolean left = false, right = false, up = false, down = false, jump = false, start = false, fall = false;
    
    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        flyCam.setMoveSpeed(100);
        
        AmbientLight bright = new AmbientLight();
        bright.setColor(ColorRGBA.White.mult(3));
        rootNode.addLight(bright);
        
        LevelCreate.setup(assetManager, bulletAppState, viewPort);
        rootNode.attachChild(LevelCreate.createLevel());

        playerControl = Player.initPlayer(playerControl,bulletAppState);
        
        setUpKeys();
    }
    
    
    private void setUpKeys(){
        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("Start", new KeyTrigger(KeyInput.KEY_Q));
        inputManager.addMapping("Fall", new KeyTrigger(KeyInput.KEY_F));
        inputManager.addListener(this, "Left","Right", "Up", "Down", "Jump", "Start", "Fall");
    }
    
    public void onAction(String name, boolean isPressed, float tpf) {
        if (name.equals("Left")){
            left = isPressed;
        }
        if (name.equals("Right")){
            right = isPressed;
        }
        if (name.equals("Up")){
            up = isPressed;
        }
        if (name.equals("Down")){
            down = isPressed;
        }
        if (name.equals("Jump")){
            jump = isPressed;
        }
        if (name.equals("Start")){
            start = isPressed;
        }
        if (name.equals("Fall")){
            fall = isPressed;
        }
    }
    
    @Override
    public void simpleUpdate(float tpf) {
        Vector3f camDir = cam.getDirection().mult(0.6f);
        camDir.setY(0);
        Vector3f camLeft = cam.getLeft().mult(0.4f);
        walkDirection.set(0,0,0);
        if (left) {walkDirection.addLocal(camLeft);}
        if (right) {walkDirection.addLocal(camLeft.negate());}
        if (up) {walkDirection.addLocal(camDir);}
        if (down) {walkDirection.addLocal(camDir.negate());}
        if (jump) {playerControl.setPhysicsLocation(new Vector3f(85, 150, 85)); 
        playerControl.setGravity(0);
        playerControl.setFallSpeed(0);}
        if (fall) {playerControl.setGravity(30); 
        playerControl.setFallSpeed(30);}
        if (start) {playerControl.setPhysicsLocation(new Vector3f(4, 5, 4)); 
        playerControl.setGravity(30); 
        playerControl.setFallSpeed(30);}
        playerControl.setWalkDirection(walkDirection);
        cam.setLocation(playerControl.getPhysicsLocation());
    }
}