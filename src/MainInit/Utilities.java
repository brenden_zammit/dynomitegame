package MainInit;

import java.util.Random;

public class Utilities {
    
    public static boolean randomChance(float chance) {
        Random random = new Random();
        if (chance > random.nextFloat()) {
            return true;
        }
        return false;
    }
    
}
