package MainInit;

import Level.LevelCreate;
import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

public class test extends SimpleApplication{

    public static void main(String[] args) {
        test app = new test();
        app.start();
    }
    
    @Override
    public void simpleInitApp() {
        
        flyCam.setMoveSpeed(100f);
    
        Spatial insideWall = assetManager.loadModel("Models/WallFinal.j3o");
        Material insideWallText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        insideWallText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/Wall1of4Final.png"));
        rootNode.attachChild(insideWall);
        
        AmbientLight l = new AmbientLight();
        //l.setDirection(Vector3f.UNIT_Y.mult(-1));
        l.setColor(ColorRGBA.White.mult(1f));
        rootNode.addLight(l);
    }
    
}
